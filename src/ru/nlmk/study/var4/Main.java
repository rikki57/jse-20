package ru.nlmk.study.var4;

import ru.nlmk.study.var4.model.SchoolClass;
import ru.nlmk.study.var4.model.Student;
import ru.nlmk.study.var4.repository.ClassRepository;
import ru.nlmk.study.var4.repository.StudentRepository;

public class Main {
    private static final String USER_NAME = "Admin";

    public static void main(String[] args) {
        ClassRepository classRepository = ClassRepository.getInstance();
        classRepository.put(new SchoolClass(USER_NAME, 1L, "8A"));
        classRepository.put(new SchoolClass(USER_NAME, 2L, "9A"));
        classRepository.put(new SchoolClass(USER_NAME, 3L, "10A"));

        StudentRepository studentRepository = StudentRepository.getInstance();
        studentRepository.put(new Student(USER_NAME, 1L));
        studentRepository.put(new Student(USER_NAME, 2L));

        classRepository.findAll(USER_NAME).forEach(System.out::println);
        studentRepository.findAll(USER_NAME).forEach(System.out::println);
    }
}
