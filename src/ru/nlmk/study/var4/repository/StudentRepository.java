package ru.nlmk.study.var4.repository;

import ru.nlmk.study.var4.model.SchoolClass;
import ru.nlmk.study.var4.model.Student;

public class StudentRepository extends AbstractRepository<Student>  {
    private static StudentRepository ourInstance = null;

    private StudentRepository() {
    }

    public static StudentRepository getInstance() {

        if (ourInstance == null) {
            ourInstance = new StudentRepository();
        }
        return ourInstance;
    }
}
