package ru.nlmk.study.var4.repository;

import ru.nlmk.study.var4.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository <E extends AbstractEntity> {
    private List<E> items = new ArrayList<>();

    public List<E> findAll(String userName){
        List<E> result = new ArrayList<>();
        for(final E item : items){
            if (userName.equals(item.getUserName())){
                result.add(item);
            }
        }
        return result;
    }

    public boolean updateById(final Long id, final E newItem, String userName){
        E oldItem = findById(id, userName);
        if (oldItem != null){
            items.remove(oldItem);
            items.add(newItem);
            return true;
        }
        return false;
    }

    public E findById(Long id, String userName){
        E result = null;
        for (final E item : items){
            if (id.equals(item.getId())){
                result = item;
                break;
            }
        }
        return result;
    }

    public void put(E value){
        items.add(value);
    }
}
