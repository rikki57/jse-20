package ru.nlmk.study.var4.repository;

import ru.nlmk.study.var4.model.SchoolClass;

public class ClassRepository extends AbstractRepository<SchoolClass> {
    private static ClassRepository ourInstance = null;

    private ClassRepository() {
    }

    public static ClassRepository getInstance() {
        if (ourInstance == null) {
            ourInstance = new ClassRepository();
        }
        return ourInstance;
    }

}
