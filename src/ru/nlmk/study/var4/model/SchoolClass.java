package ru.nlmk.study.var4.model;

public class SchoolClass extends AbstractEntity{
    private String className;

    public SchoolClass(String userName, Long id, String className) {
        super(userName, id);
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public String toString() {
        return "SchoolClass{" +
                "className='" + className + '\'' +
                '}';
    }
}
