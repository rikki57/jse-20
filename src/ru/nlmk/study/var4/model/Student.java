package ru.nlmk.study.var4.model;

public class Student extends AbstractEntity{
    private String studentName;
    private SchoolClass schoolClass;

    public Student(String userName, Long id) {
        super(userName, id);
    }


    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public SchoolClass getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(SchoolClass schoolClass) {
        this.schoolClass = schoolClass;
    }
}
