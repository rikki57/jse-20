package ru.nlmk.study.var4.model;

public abstract class AbstractEntity {
    private String userName;
    private Long id;

    public AbstractEntity(String userName, Long id) {
        this.userName = userName;
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AbstractEntity{" +
                "userName='" + userName + '\'' +
                ", id=" + id +
                '}';
    }
}
