package ru.nlmk.study.var3;

public interface Subscriber {
    public void update(Race race);
}
