package ru.nlmk.study.var3;

public interface Publisher {
    public void addRacer(Racer racer);

    public void deleteRacer(Racer racer);

    public void notifyRacers(Race race);
}
