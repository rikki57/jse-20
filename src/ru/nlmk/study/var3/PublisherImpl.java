package ru.nlmk.study.var3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PublisherImpl implements Publisher {
    Set<Racer> racers = new HashSet<>();

    public void addRacer(Racer racer) {
        racers.add(racer);
    }

    public void deleteRacer(Racer racer) {
        racers.remove(racer);
    }

    public void notifyRacers(Race race) {
        for (Racer racer : racers) {
            racer.update(race);
        }
    }
}
