package ru.nlmk.study.var3;

public class Race {
    private String info;

    public Race(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
