package ru.nlmk.study.var3;

public class Main {
    public static void main(String[] args) {
        Publisher raceInformator = new PublisherImpl();

        Racer racer = new Racer("Jilles Villeneuve");
        raceInformator.addRacer(racer);
        racer = new Racer("Ayrton Senna");
        raceInformator.addRacer(racer);
        racer = new Racer("Justin Willson");
        raceInformator.addRacer(racer);
        racer = new Racer("Bruce Mclaren");
        raceInformator.addRacer(racer);
        raceInformator.addRacer(racer);

        raceInformator.notifyRacers(new Race("Monza, 20th of may"));

        System.out.println("");
        raceInformator.deleteRacer(racer);
        raceInformator.notifyRacers(new Race("Monaco, 27th of may"));
    }
}
