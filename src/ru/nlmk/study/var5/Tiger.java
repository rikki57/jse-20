package ru.nlmk.study.var5;

public class Tiger extends Animal{
    public Tiger(String name, double weight) {
        super(name, weight);
    }

    @Override
    public void makeNoise() {
        System.out.println("Rrrrrrrr");
    }
}
