package ru.nlmk.study.var5;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Animal> animals = new ArrayList();
        animals.add(new Tamagochi("barsik", 0.01));
        animals.add(new Tiger("totoshka", 200.01));
        executeZoo(animals);

        List<Tamagochi> tamagochis = new ArrayList<>();
        tamagochis.add(new Tamagochi("barsik", 0.01));
        tamagochis.add(new Tamagochi("totoshka", 0.01));
        executeZoo(tamagochis);

        List<Tiger> tigers = new ArrayList<>();
        tigers.add(new Tiger("barsik", 0.01));
        tigers.add(new Tiger("totoshka", 0.01));
        executeZoo(tigers);

        List list = new ArrayList();
        list.add("1");
    }

    private static void executeZoo(List<? extends Animal> animals){
        for (Animal animal : animals){
            ((Animal) animal).makeNoise();
        }
    }
}
