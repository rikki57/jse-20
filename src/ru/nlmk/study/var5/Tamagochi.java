package ru.nlmk.study.var5;

public class Tamagochi extends Animal {
    public Tamagochi(String name, double weight) {
        super(name, weight);
    }

    @Override
    public void makeNoise() {
        System.out.println("pipipi");
    }
}
