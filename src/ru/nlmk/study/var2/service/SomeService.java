package ru.nlmk.study.var2.service;


import ru.nlmk.study.var2.repository.ISomeRespository;
import ru.nlmk.study.var2.repository.SomeRepository;

public class SomeService implements ISomeService {
    private final ISomeRespository someRespository;
    private static SomeService ourInstance = null;

    private SomeService(ISomeRespository respository) {
        someRespository = respository;
    }

    public static SomeService getInstance(ISomeRespository respository) {
        if (ourInstance == null) {
            ourInstance = new SomeService(respository);
        }
        return ourInstance;
    }

    @Override
    public String makeServiceWork(String request) {
        System.out.println("Make service work: " + request);
        String res = someRespository.getData(request);
        System.out.println("received data: " + res);
        return res;
    }
}
