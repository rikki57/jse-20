package ru.nlmk.study.var2.service;

public interface ISomeService {
    String makeServiceWork(String request);
}
