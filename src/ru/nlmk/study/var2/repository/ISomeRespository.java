package ru.nlmk.study.var2.repository;

public interface ISomeRespository {
    String getData(String id);
}
