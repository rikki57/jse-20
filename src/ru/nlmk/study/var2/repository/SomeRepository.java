package ru.nlmk.study.var2.repository;

import ru.nlmk.study.var1.SingletonPattern;

public class SomeRepository implements ISomeRespository{
    private static SomeRepository ourInstance = null;

    private SomeRepository() {
    }

    public static SomeRepository getInstance() {

        if (ourInstance == null) {
            ourInstance = new SomeRepository();
        }
        return ourInstance;
    }

    @Override
    public String getData(String id) {
        return "data";
    }
}
