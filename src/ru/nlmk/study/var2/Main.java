package ru.nlmk.study.var2;

import ru.nlmk.study.var2.controller.ISomeController;
import ru.nlmk.study.var2.controller.SomeController;
import ru.nlmk.study.var2.repository.SomeRepository;
import ru.nlmk.study.var2.service.SomeService;

public class Main {
    public static void main(String[] args) {
        ISomeController controller = SomeController.getInstance(
                SomeService.getInstance(
                        SomeRepository.getInstance()));
        controller.executeCommand("1");
        controller.executeCommand("2");
    }
}
