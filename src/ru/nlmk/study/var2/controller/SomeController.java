package ru.nlmk.study.var2.controller;

import ru.nlmk.study.var2.repository.SomeRepository;
import ru.nlmk.study.var2.service.ISomeService;
import ru.nlmk.study.var2.service.SomeService;

public class SomeController implements ISomeController{
    private static SomeController ourInstance = null;
    private final ISomeService someService;

    private SomeController(ISomeService service) {
        someService = service;
    }

    public static SomeController getInstance(ISomeService service) {
        if (ourInstance == null) {
            ourInstance = new SomeController(service);
        }
        return ourInstance;
    }

    @Override
    public String executeCommand(String command) {
        System.out.println("Called command: " + command);
        return someService.makeServiceWork(command);
    }
}
