package ru.nlmk.study.var2.controller;

public interface ISomeController {
    String executeCommand(String command);
}
