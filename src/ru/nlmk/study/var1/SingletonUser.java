package ru.nlmk.study.var1;

public class SingletonUser {
    private final SingletonPattern singletonPattern = SingletonPattern.getInstance();

    public void useSingleton(Integer val){
        singletonPattern.useSingleton(val);
    }
}
