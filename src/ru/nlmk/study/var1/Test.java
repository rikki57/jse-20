package ru.nlmk.study.var1;

public class Test {
    public static void main(String[] args) {
        SingletonUser singletonUser = new SingletonUser();
        singletonUser.useSingleton(5);
        singletonUser.useSingleton(16);
    }
}
