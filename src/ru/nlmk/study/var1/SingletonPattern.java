package ru.nlmk.study.var1;

public class SingletonPattern {
    private static SingletonPattern ourInstance = null;
    private Integer value;

    private SingletonPattern() {
    }

    public static SingletonPattern getInstance() {

            if (ourInstance == null) {
                ourInstance = new SingletonPattern();
            }
            return ourInstance;
    }

    public void useSingleton(Integer val){
        value = val;
        System.out.println(value);
    }
}
